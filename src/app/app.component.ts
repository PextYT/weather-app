import { Component, OnInit,   } from '@angular/core';
import {WeatherService} from "./weather.service";
import {WeatherModel} from "./weather.model";
import {WeatherList} from "./weatherList.model";

import { DatePipe } from '@angular/common';
import {compareNumbers} from "@angular/compiler-cli/src/diagnostics/typescript_version";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [DatePipe]
})
export class AppComponent implements OnInit{

  public LIST: Array<WeatherList>;
  public counter: number = 0;
  public counter2: number = this.counter;
  public tempArr: Array<WeatherList>;
  public tempDate: string;
  myDate: {} = new Date();

  constructor(private _WeatherService: WeatherService, private datePipe: DatePipe) {
    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  }
  isBigEnough(element, index, array) {
    return (element == this.myDate);
  }
  ngOnInit(){
    this._WeatherService.getWeather(8049, "ch").subscribe(data => {
      this.LIST = data.list;
      for (let i in this.LIST){
        this.tempDate = this.datePipe.transform(this.LIST[i].dt_txt, 'yyyy-MM-dd');
        if (this.tempDate == this.myDate){
          this.counter++
          console.log(this.counter);
        } else if (this.tempDate >= this.myDate){
          this.counter2 = this.counter+7;
          this.counter2++
          console.log(this.counter2);
          break;
        }
      }
    })
  }
  tomorowClicked() {
    for (let i in this.LIST) {
      this.tempDate = this.datePipe.transform(this.LIST[i].dt_txt, 'yyyy-MM-dd');
      if (this.tempDate == this.myDate) {
        this.counter++
      } else if (this.tempDate >= this.myDate) {
        this.counter = this.counter2;
        this.counter2 = this.counter2+7;
        this.counter2++
        console.log("counter:" + this.counter);
        console.log("counter2:" + this.counter2);
        break;
      }
    }
  }
  yesterdayClicked() {
    for (let i in this.LIST) {
      this.tempDate = this.datePipe.transform(this.LIST[i].dt_txt, 'yyyy-MM-dd');
      if (this.tempDate >= this.myDate) {
        this.counter = this.counter-8;
        this.counter2 = this.counter2-7;
        this.counter2--
        console.log("counter:" + this.counter);
        console.log("counter2:" + this.counter2);
        break;
      }
    }
  }
}
