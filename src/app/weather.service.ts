import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {WeatherModel} from "./weather.model";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private apiUrl = 'http://api.openweathermap.org/data/2.5/';
  private key = '&appid=f76bee050741ed291f08eb2778b34a9d';
  private languageEn = '&lang=en';
  private celsius = '&units=metric';
  private date = '&units=utc';

  constructor(private http: HttpClient) {
  }

  getWeather(zipCode: number, countryCode: string): Observable<WeatherModel> {
    const fullUrl = this.apiUrl + 'forecast?zip=' + zipCode + ',' + countryCode + this.key + this.celsius + this.date;
    console.log(fullUrl);
    return this.http.get<any>(fullUrl);
  }
}
