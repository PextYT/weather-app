export  interface WeatherList {
  main: {
    temp: number;
  };
  weather: [
    {
      icon: string;
    }
  ],
  wind: {
    speed: number;
  }
  dt_txt: string;
}
